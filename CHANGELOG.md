CHANGELOG
=========

4.0.0
-----
- Requiert la bibliotèque unicaen/privilege.

3.0.0
-----
- Requiert PHP 8.0

2.0.2
-----
- [FIX] livelog-panel.phtml : l'URL de connexion à la websocket est systématiquement en 'wss://'.

2.0.1
-----
- [FIX] PHP 7.3+ interdit par erreur.
- [FIX] Suppression du type de route console unicaen qui provoque en PHP 7.4 l'erreur 'A plugin by the name UnicaenConsoleRouterSimple was not found'.
- [FIX] Trop tôt pour obliger PHP 8 dans le master.

2.0.0
-----
- SUppression de la dépendance avec unicaen/app
- [FIX] CHemins liés à unicaen/console remplaçant laminas/console

1.0.0
-----
- Premmière version stable.
