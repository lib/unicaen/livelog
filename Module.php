<?php

namespace UnicaenLivelog;

use Laminas\Config\Factory as ConfigFactory;
use Unicaen\Console\Adapter\AdapterInterface;
use Laminas\Filter\Word\CamelCaseToDash;
use Laminas\Mvc\ModuleRouteListener;
use Laminas\Mvc\MvcEvent;
use Laminas\Stdlib\Glob;

class Module
{
//    public function onBootstrap(MvcEvent $e)
//    {
//        $eventManager        = $e->getApplication()->getEventManager();
//        $moduleRouteListener = new ModuleRouteListener();
//        $moduleRouteListener->attach($eventManager);
//    }

    public function getConfig()
    {
        $paths = array_merge(
            [__DIR__ . '/config/module.config.php'],
            Glob::glob(__DIR__ . '/config/others/{,*.}{config}.php', Glob::GLOB_BRACE)
        );

        return ConfigFactory::fromFiles($paths);
    }

    public function getAutoloaderConfig(): array
    {
        return array(
            'Laminas\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @inheritDoc
     */
    public function getConsoleBanner(AdapterInterface $console): ?string
    {
        return 'Module ' . __NAMESPACE__;
    }

    /**
     * @inheritDoc
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return [
            /**
             * @see ConsoleController::runSocketsAction()
             */
            "unicaen-livelog run-sockets [--verbose=<verbose>]" => "Lance les 2 serveurs (socket et websocket).",
            [ '<verbose>', "Mode verbeux (1) ou pas (0).", "Facultatif"],

            /**
             * @see ConsoleController::testAction()
             */
            "unicaen-livelog test [--message=<message>]" => "Teste l'envoi d'un message au serveur de socket.",
            [ '<message>', "Message de log", "Facultatif"],
        ];
    }
}
