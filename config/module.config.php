<?php

namespace UnicaenLivelog;

use UnicaenLivelog\Controller\ConsoleController;
use UnicaenLivelog\Controller\ConsoleControllerFactory;
use UnicaenLivelog\Controller\IndexController;
use UnicaenLivelog\Controller\IndexControllerFactory;
use UnicaenLivelog\Log\LivelogSocketLogHandler;
use UnicaenLivelog\Log\LivelogSocketLogHandlerFactory;
use UnicaenLivelog\Session\LivelogSessionContainer;
use UnicaenLivelog\Session\LivelogSessionContainerFactory;
use UnicaenLivelog\Socket\LivelogSocket;
use UnicaenLivelog\Socket\LivelogSocketFactory;
use UnicaenLivelog\Socket\LivelogWebsocket;
use UnicaenLivelog\Socket\LivelogWebsocketFactory;
use UnicaenLivelog\View\Helper\LivelogPanelHelper;
use UnicaenLivelog\View\Helper\LivelogPanelHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'unicaen-livelog' => [

    ],

    'router' => [
        'routes' => [
            'application' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/UnicaenLivelog',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [

                ],
            ],
        ],
    ],
    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [

                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'run-sockets' => [
                    'options' => [
                        'route' => 'unicaen-livelog run-sockets [--verbose=]',
                        'defaults' => [
                            /**
                             * @see ConsoleController::runSocketsAction()
                             */
                            'controller' => ConsoleController::class,
                            'action' => 'run-sockets',
                        ],
                    ],
                ],
                'test' => [
                    'options' => [
                        'route' => 'unicaen-livelog test [--message=]',
                        'defaults' => [
                            /**
                             * @see ConsoleController::testAction()
                             */
                            'controller' => ConsoleController::class,
                            'action' => 'test',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    /**
                     * @see ConsoleController::genererXmlAction()
                     * @see ConsoleController::genererTefAction()
                     * @see ConsoleController::deposerAction()
                     */
                    'controller' => ConsoleController::class,
                    'action' => [
                        'run-sockets',
                        'test',
                    ],
                    'role' => [],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            // server
            LivelogWebsocket::class => LivelogWebsocketFactory::class,
            LivelogSocket::class => LivelogSocketFactory::class,
            // log
            LivelogSocketLogHandler::class => LivelogSocketLogHandlerFactory::class,
            // session
            LivelogSessionContainer::class => LivelogSessionContainerFactory::class,

        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
            ConsoleController::class => ConsoleControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'view_helpers' => [
        'aliases' => [
            'livelogPanel' => LivelogPanelHelper::class,
        ],
        'factories' => [
            LivelogPanelHelper::class => LivelogPanelHelperFactory::class,
        ],
    ],
];