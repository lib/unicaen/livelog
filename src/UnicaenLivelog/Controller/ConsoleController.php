<?php

namespace UnicaenLivelog\Controller;

use Unicaen\Console\Controller\AbstractConsoleController;
use Monolog\Logger;
use React\EventLoop\Loop;
use UnicaenLivelog\Log\LivelogSocketLogHandlerAwareTrait;
use UnicaenLivelog\Socket\LivelogSocket;
use UnicaenLivelog\Socket\LivelogWebsocket;

class ConsoleController extends AbstractConsoleController
{
    use LivelogSocketLogHandlerAwareTrait;

    /**
     * @var LivelogWebsocket
     */
    protected $webSocket;

    /**
     * @var \UnicaenLivelog\Socket\LivelogSocket
     */
    protected $socket;

    /**
     * @param \UnicaenLivelog\Socket\LivelogWebsocket $webSocket
     * @return self
     */
    public function setWebSocket(LivelogWebsocket $webSocket): self
    {
        $this->webSocket = $webSocket;
        return $this;
    }

    /**
     * @param \UnicaenLivelog\Socket\LivelogSocket $socket
     * @return self
     */
    public function setSocket(LivelogSocket $socket): self
    {
        $this->socket = $socket;
        return $this;
    }

    public function runSocketsAction()
    {
        $verbose = (bool) $this->params('verbose', 0);

        $this->runSockets($verbose);
    }

    protected function runSockets(bool $verbose)
    {
        $this->socket->setVerbose($verbose);
        $this->webSocket->setVerbose($verbose);

        $loop = Loop::get();
        echo "Running WebSocket at {$this->webSocket->getPrivateUrl()}..." . PHP_EOL;
        $this->webSocket->run([], $loop);
        echo "Running Socket at {$this->socket->getPath()}..." . PHP_EOL;
        $this->socket->run($loop);
        $loop->run();
    }

    public function testAction()
    {
        $message = $this->params('message', "Ceci est un test à 2$ ! D'ac ? ~ %");

        $logger = new Logger('mylogger');
        $logger->pushHandler($this->livelogSocketHandler);
        $logger->info($message);
    }
}