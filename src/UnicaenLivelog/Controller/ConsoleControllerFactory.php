<?php

namespace UnicaenLivelog\Controller;

use Interop\Container\ContainerInterface;
use UnicaenLivelog\Log\LivelogSocketLogHandler;
use UnicaenLivelog\Socket\LivelogSocket;
use UnicaenLivelog\Socket\LivelogWebsocket;

class ConsoleControllerFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return ConsoleController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ConsoleController
    {
        $controller = new ConsoleController();

        /** @var \UnicaenLivelog\Socket\LivelogWebsocket $webSocketUrl */
        $webSocketUrl = $container->get(LivelogWebsocket::class);
        /** @var LivelogSocket $socketUrl */
        $socketUrl = $container->get(LivelogSocket::class);

        $livelogSocketHandler = $container->get(LivelogSocketLogHandler::class);

        $controller->setWebSocket($webSocketUrl);
        $controller->setSocket($socketUrl);
        $controller->setLivelogSocketHandler($livelogSocketHandler);

        return $controller;
    }
}