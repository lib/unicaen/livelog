<?php

namespace UnicaenLivelog\Log;

use Monolog\Handler\SocketHandler;
use Monolog\Logger;
use UnicaenLivelog\Session\LivelogSessionContainer;

class LivelogSocketLogHandler extends SocketHandler
{
    /**
     * @var LivelogSessionContainer
     */
    protected $sessionContainer;

    /**
     * @param string $connectionString
     * @param int $level
     * @param bool $bubble
     */
    public function __construct(string $connectionString, $level = Logger::DEBUG, bool $bubble = true)
    {
        parent::__construct($connectionString, $level, $bubble);

        $this->sessionContainer = new LivelogSessionContainer();
    }

    /**
     * @param LivelogSessionContainer $sessionContainer
     * @return self
     */
    public function setSessionContainer(LivelogSessionContainer $sessionContainer): self
    {
        $this->sessionContainer = $sessionContainer;
        return $this;
    }

    /**
     * @param array $record
     */
    protected function write(array $record): void
    {
        try {
            parent::write($record);
        } catch (\UnexpectedValueException $e) {
            // Un problème avec la socket ne doit pas être bloquant...
            // ex : "Failed connecting to unix:///tmp/unicaen_livelog.sock (111: Connection refused)"
            error_log($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 0);
        }
    }

    /**
     * @inheritDoc
     */
    protected function generateDataStream(array $record): string
    {
        return json_encode([
            'formatted' => parent::generateDataStream($record),
            'clientId' => $this->sessionContainer->getClientId(),
        ]);
    }
}