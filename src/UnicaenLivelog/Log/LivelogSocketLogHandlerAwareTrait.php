<?php

namespace UnicaenLivelog\Log;

trait LivelogSocketLogHandlerAwareTrait
{
    /**
     * @var \UnicaenLivelog\Log\LivelogSocketLogHandler
     */
    protected $livelogSocketHandler;

    /**
     * @param \UnicaenLivelog\Log\LivelogSocketLogHandler $livelogSocketHandler
     */
    public function setLivelogSocketHandler(LivelogSocketLogHandler $livelogSocketHandler)
    {
        $this->livelogSocketHandler = $livelogSocketHandler;
    }
}