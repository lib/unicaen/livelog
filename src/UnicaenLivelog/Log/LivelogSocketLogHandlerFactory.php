<?php

namespace UnicaenLivelog\Log;

use Psr\Container\ContainerInterface;
use UnicaenLivelog\Socket\LivelogSocket;
use UnicaenLivelog\Session\LivelogSessionContainer;

class LivelogSocketLogHandlerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): LivelogSocketLogHandler
    {
        $socketUrl = $container->get(LivelogSocket::class);
        $sessionContainer = $container->get(LivelogSessionContainer::class);

        $handler = new LivelogSocketLogHandler($socketUrl->getPath());
        $handler->setSessionContainer($sessionContainer);

        return $handler;
    }
}