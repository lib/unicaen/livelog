<?php

namespace UnicaenLivelog\Session;

use Laminas\Session\Container;
use Laminas\Session\ManagerInterface;

class LivelogSessionContainer extends Container
{
    const CLIENT_ID_SESSION_KEY = 'clientId';

    public function __construct($name = __NAMESPACE__, ?ManagerInterface $manager = null)
    {
        parent::__construct($name, $manager);
    }

    public function getClientId(): string
    {
        $key = self::CLIENT_ID_SESSION_KEY;

        if (! $this->offsetGet($key)) {
            $this->offsetSet($key, $this->generateClientId());
        }

        return $this->offsetGet($key);
    }

    protected function generateClientId(): string
    {
        return uniqid('client_');
    }
}