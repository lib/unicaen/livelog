<?php

namespace UnicaenLivelog\Session;

use Laminas\Session\Container;
use Psr\Container\ContainerInterface;

class LivelogSessionContainerFactory
{
    public function __invoke(ContainerInterface $container): Container
    {
        $sessionContainer = new LivelogSessionContainer(__NAMESPACE__);
        $sessionContainer->setExpirationSeconds(60*60*2); // 2h

        return $sessionContainer;
    }
}