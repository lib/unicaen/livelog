<?php

namespace UnicaenLivelog\Socket;

use React\EventLoop\LoopInterface;
use React\Socket\ConnectionInterface;
use React\Socket\SocketServer;

class LivelogSocket
{
    /**
     * @var string
     */
    private $path; // ex : 'unix:///tmp/server.sock'

    /**
     * @var LivelogWebsocket
     */
    private $webSocket;

    /**
     * @var bool
     */
    private $verbose = false;

    /**
     * @param string $path
     * @param \UnicaenLivelog\Socket\LivelogWebsocket $webSocket
     */
    public function __construct(string $path, LivelogWebsocket $webSocket)
    {
        $this->path = $path;
        $this->webSocket = $webSocket;
    }

    /**
     * @param bool $verbose
     */
    public function setVerbose(bool $verbose = true)
    {
        $this->verbose = $verbose;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param \React\EventLoop\LoopInterface|null $loop
     */
    public function run(LoopInterface $loop = null)
    {
        $socket = new SocketServer($this->path, [], $loop);
        $socket->on('connection', function (ConnectionInterface $connection) {
            $this->debug("CONNECTION " . $connection->getRemoteAddress());
            $connection->on('data', function ($data) {
                $this->debug("DATA " . $data);
                ['formatted' => $line, 'clientId' => $clientId] = json_decode($data, true);
                if ($line === null) {
                    $line = $data;
                }
                // on passe directement le plat à la websocket
                $this->webSocket->send($line, $clientId);
            });
        });
        $socket->on('close', function (ConnectionInterface $connection) {
            $this->debug("CLOSE " . $connection->getRemoteAddress());
        });
        $this->debug("RUN");
    }

    protected function debug(string $message)
    {
        if ($this->verbose) {
            echo '[socket]>    ' . $message . PHP_EOL;
        }
    }
}