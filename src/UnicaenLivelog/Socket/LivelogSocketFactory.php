<?php

namespace UnicaenLivelog\Socket;

use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class LivelogSocketFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): LivelogSocket
    {
        $config = $this->getConfig($container);
        Assert::keyExists($config, 'path');
        Assert::stringNotEmpty($config['path']);
        $path = $config['path'];
        $verbose = $config['verbose'] ?? false;

        /** @var LivelogWebsocket $config */
        $webSocket = $container->get(LivelogWebsocket::class);

        $server = new LivelogSocket($path, $webSocket);
        $server->setVerbose($verbose);

        return $server;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getConfig(ContainerInterface $container): array
    {
        /** @var array $config */
        $config = $container->get('Config');

        Assert::keyExists($config, $key = 'unicaen-livelog', "La clé de config '$key' est introuvable");
        $config = $config[$key];
        Assert::keyExists($config, 'socket');

        return $config['socket'];
    }
}