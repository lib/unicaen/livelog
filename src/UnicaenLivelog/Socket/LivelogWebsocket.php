<?php

namespace UnicaenLivelog\Socket;

use Exception;
use Psr\Http\Message\RequestInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\MessageComponentInterface;
use Ratchet\WebSocket\WsServer;
use Ratchet\WebSocket\WsServerInterface;
use React\EventLoop\LoopInterface;
use React\Socket\SocketServer;
use Webmozart\Assert\Assert;

class LivelogWebsocket implements MessageComponentInterface, WsServerInterface
{
    const CLIENT_ID_QUERY_PARAM = 'client';

    /**
     * @var string
     */
    protected $privateUrl;

    /**
     * @var string
     */
    protected $publicUrl;

    /**
     * @var ConnectionInterface[]
     */
    protected $clients;

    /**
     * @var bool
     */
    protected $verbose = false;

    /**
     * @param string $privateUrl
     * @param string $publicUrl
     */
    public function __construct(string $privateUrl, string $publicUrl)
    {
        $this->privateUrl = $privateUrl;
        $this->publicUrl = $publicUrl;
        $this->clients = [];
    }

    /**
     * @param bool $verbose
     */
    public function setVerbose(bool $verbose = true)
    {
        $this->verbose = $verbose;
    }

    /**
     * @inheritDoc
     */
    function getSubProtocols(): array
    {
        return ['livelog'];
    }

    /**
     * @return string
     */
    public function getPrivateUrl(): string
    {
        return $this->privateUrl;
    }

    /**
     * @return string
     */
    public function getPublicUrl(): string
    {
        return $this->publicUrl;
    }

    /**
     * @param array $context
     * @param \React\EventLoop\LoopInterface|null $loop
     */
    public function run(array $context = [], LoopInterface $loop = null)
    {
        $server = new SocketServer($this->privateUrl, $context, $loop);
        $httpServer = new HttpServer(new WsServer($this));
        new IoServer($httpServer, $server, $loop);
        $this->debug("RUN");
    }

    /**
     * @param string $data
     * @param string|null $clientId
     */
    public function send(string $data, string $clientId = null)
    {
        if ($clientId !== null) {
            if (isset($this->clients[$clientId])) {
                $this->debug("SEND Client $clientId : " . $data);
                $this->clients[$clientId]->send($data);
            } else {
                $this->debug("SEND : Stop, $clientId inconnu !");
            }
        } else {
            foreach ($this->clients as $client) {
                $this->debug("SEND Clients : " . $data);
                $client->send($data);
            }
        }
    }

    /**
     * @param \Ratchet\WebSocket\WsConnection $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $clientId = $this->extractClientIdFromConnection($conn);
        $this->clients[$clientId] = $conn;
        $this->debug('OPEN : ' . $clientId);
    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $this->debug('MESSAGE : ' . $msg);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $clientId = $this->extractClientIdFromConnection($conn);
        unset($this->clients[$clientId]);
        $this->debug('CLOSE : ' . $clientId);
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
        $conn->close();
        $this->debug('ERROR : ' . $e->getMessage());
    }

    protected function extractClientIdFromConnection(ConnectionInterface $conn): string
    {
        $query = $this->getRequestFromConnection($conn)->getUri()->getQuery();
        parse_str($query, $queryParts);
        Assert::keyExists($queryParts, $key = self::CLIENT_ID_QUERY_PARAM, "La requête doit contenir le paramètre GET suivant : " . $key);

        return $queryParts[$key];
    }

    protected function getRequestFromConnection(ConnectionInterface $conn): RequestInterface
    {
        return $conn->{'httpRequest'};
    }

    protected function debug(string $message)
    {
        if ($this->verbose) {
            echo '[websocket]> ' . $message . PHP_EOL;
        }
    }
}
