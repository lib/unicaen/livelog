<?php

namespace UnicaenLivelog\Socket;

use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class LivelogWebsocketFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): LivelogWebsocket
    {
        $config = $this->getConfig($container);
        Assert::keyExists($config, 'private_url');
        Assert::keyExists($config, 'public_url');
        Assert::stringNotEmpty($config['private_url']);
        Assert::stringNotEmpty($config['public_url']);
        $privateUrl = $config['private_url'];
        $publicUrl = $config['public_url'];
        $verbose = $config['verbose'] ?? false;

        $server = new LivelogWebsocket($privateUrl, $publicUrl);
        $server->setVerbose($verbose);

        return $server;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getConfig(ContainerInterface $container): array
    {
        /** @var array $config */
        $config = $container->get('Config');

        Assert::keyExists($config, $key = 'unicaen-livelog', "La clé de config '$key' est introuvable");
        $config = $config[$key];
        Assert::keyExists($config, 'websocket');

        return $config['websocket'];
    }
}