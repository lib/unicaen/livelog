<?php

namespace UnicaenLivelog\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use UnicaenLivelog\Session\LivelogSessionContainer;

/**
 * @property \Laminas\View\Renderer\PhpRenderer $view
 */
class LivelogPanelHelper extends AbstractHelper
{
    /**
     * @var string
     */
    protected $divId = 'livelog-div';

    /**
     * @var string
     */
    protected $websocketPublicUrl;

    /**
     * @var LivelogSessionContainer
     */
    protected $sessionContainer;

    /**
     * @param string $websocketPublicUrl
     * @param \UnicaenLivelog\Session\LivelogSessionContainer $sessionContainer
     */
    public function __construct(string $websocketPublicUrl, LivelogSessionContainer $sessionContainer)
    {
        $this->websocketPublicUrl = $websocketPublicUrl;
        $this->sessionContainer = $sessionContainer;
    }

    /**
     * @return self
     */
    public function __invoke(): self
    {
        return $this;
    }

    /**
     * @param string $divId
     * @return \UnicaenLivelog\View\Helper\LivelogPanelHelper
     */
    public function setDivId(string $divId): self
    {
        $this->divId = $divId;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->view->partial('unicaen-livelog/livelog-panel', [
            'url' => $this->websocketPublicUrl,
            'clientId' => $this->sessionContainer->getClientId(),
            'divId' => $this->divId,
        ]);
    }
}