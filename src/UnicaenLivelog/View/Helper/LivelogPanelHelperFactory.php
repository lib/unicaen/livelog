<?php

namespace UnicaenLivelog\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenLivelog\Socket\LivelogWebsocket;
use UnicaenLivelog\Session\LivelogSessionContainer;

class LivelogPanelHelperFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): LivelogPanelHelper
    {
        $webSocket = $container->get(LivelogWebsocket::class);
        $sessionContainer = $container->get(LivelogSessionContainer::class);

        return new LivelogPanelHelper($webSocket->getPublicUrl(), $sessionContainer);
    }
}